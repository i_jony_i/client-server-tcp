#include <stdio.h>
#include <sys/socket.h>
#include <resolv.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

int main ()
{
    /*creating sokcet and check it for errors*/
    int sd =socket(AF_INET, SOCK_STREAM,0);
    if(sd<0)
    {
            perror("Socket");
            return errno;
    }
    /*Creating socket structure, fill ti with zeros, and write data to it*/
    struct sockaddr_in addr;
    memset(&addr,0,sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(4000);
    addr.sin_addr.s_addr = INADDR_ANY;
    /*Binding defined port to a socket and check for errors*/
	if( bind(sd, &addr,sizeof(addr)) != 0)
    {
        perror("Bind");
        return errno;
    }
    /*making server listen on socket and chek for errors*/
	if(listen(sd,20) != 0)
    {
	       perror("listen");
           return(errno);
    }
    /*awaiting for connection*/
	while(1)
	{
   		int size = sizeof(addr);
        int clientsd = accept (sd, &addr, &size);
        /*recived connection handler.*/
		if (clientsd > 0)
		{
            /*
             * Creating recive/send buffer, recived bytes integer,
             * output buffer, buffer char, lenght of the sending data
             */
			char buffer[1024];
			int nbytes;
            char output[1024];
            char c;
            int l;
            /*This part prints recived data and awaits server to send his data to client*/
			while(1)
			{
                /*testing recived message lenght, and print it*/
				memset(&buffer,0,sizeof(buffer));
				if(nbytes = recv(clientsd,buffer,sizeof(buffer), 0) < 0)
                {
                    perror("Recived govno");
                    return(errno);
                }
				printf(buffer); 
                printf("\n");
                if(strcmp("bye",strncpy(buffer,3))!=0)
                {
                    close(clientsd);
                    break;
                }
                /*awaiting for server to input data and send it back to client*/
                memset(&output,0,sizeof(output));
                l=0;
				while((c=getchar()) != '\n')
                {
                    output[l]=c;
                    l++;
                }
				send(clientsd,output,l,0);	
            }        
        }
		else
			perror("Accept");
	}	
       return 0;
}

