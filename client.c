#include <stdlib.h>
#include <sys/socket.h>
#include <resolv.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <arpa/inet.h>

int main(int argc, char *argv[])
{
    /*Arguments checking*/
    if(argc != 3)
    {
        printf("Wrond arguments.\n");
        printf("USAGE: run_client \[Server IP\], \[Server Port\]\n");
        return 228;
    }
    /*Opening new socket*/
    int sd=socket(AF_INET,SOCK_STREAM,0);
    if(sd<0)
    {
        perror("socket error");
        return(errno);
    }
    /*Filling structure for connection*/
    int port = atoi(argv[2]);
    printf("%d\n", port);
    struct sockaddr_in dest;
    memset(&dest,0,sizeof(dest));
    dest.sin_family = AF_INET;
    dest.sin_port = htons(port);
    inet_aton(argv[1], &dest.sin_addr);
    /*Connect to the server*/
    if (connect(sd,&dest,sizeof(dest)) !=0)
    {
        perror("Connection problem");
        abort();
    }
    char input[1024];
    int l;
    char c;
    char buffer[1024];
    int readed;
    while(1)
    {
        l=0;
        memset(&input,0,sizeof(input));
		while((c=getchar()) != '\n')
        {
            input[l]=c;
            l++;
        }
        send(sd,input,l,0);
        memset(&buffer,0,sizeof(buffer));
        if (readed=recv(sd,buffer, sizeof(buffer),0)<0)
        {
            perror("Recive Error");
        }
        printf(buffer);
        printf("\n");
    }
}
